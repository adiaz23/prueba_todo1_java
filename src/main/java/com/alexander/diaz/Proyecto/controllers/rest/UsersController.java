/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.controllers.rest;

import com.alexander.diaz.Proyecto.model.dto.Result;
import com.alexander.diaz.Proyecto.model.dto.UserPermisson;
import com.alexander.diaz.Proyecto.model.entities.Usuarios;
import com.alexander.diaz.Proyecto.services.UserService;
import com.alexander.diaz.Proyecto.services.interfaces.UsuarioRespository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jadiaz
 */

@RestController
@CrossOrigin(origins = "*")
public class UsersController {
    
    @Autowired
    private UserService userService;
    
    @GetMapping("/getUsuarios")
    public Result getUsuarios(){
        
        Result result = new Result();
        result.setData(userService.findAll());
        result.setResult(true);
        result.setMessage("");
        result.setError("");
            
        return result;
    }
    
    @GetMapping("/getLogin")
    public Result getLogin(
            @RequestParam(value="user") String user,
            @RequestParam(value="password") String password){
        Result result = new Result();
        
        String clave = DigestUtils.sha256Hex(password);
        
        if(true){
            result.setData(userService.getLogin(user, clave));
            result.setResult(true);
            result.setMessage("");
            result.setError("");
        }else{
            result.setData(null);
            result.setResult(false);
            result.setMessage("No Data");
            result.setError("500");
        }
        
        return result;
    }
    
}
