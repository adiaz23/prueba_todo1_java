/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.controllers.rest;

import com.alexander.diaz.Proyecto.model.dto.Result;
import com.alexander.diaz.Proyecto.model.entities.Productos;
import com.alexander.diaz.Proyecto.services.ProductosService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jadiaz
 */
@RestController
@CrossOrigin(origins = "*")
public class ProductosController {
    
    @Autowired
    ProductosService productosService;
    
    
    @GetMapping("/getProductos")
    public Result getProductos(){
    
        Result result = new Result();
        
        result.setData(productosService.findAll());
        result.setResult(true);
        result.setMessage("");
        result.setError("");
            
        return result;
    }
    
    @PostMapping("/saveProductos")
    public Result save(@RequestParam Map<String, String> producto){
        
        Result result = new Result();
        
        Productos prod = new Productos(
                producto.get("nombre")!=null?producto.get("nombre"):"", 
                producto.get("descripcion")!=null?producto.get("descripcion"):"", 
                producto.get("precioVenta")!=null?Integer.parseInt(producto.get("precioCompra")):0, 
                producto.get("precioVenta")!=null?Integer.parseInt(producto.get("precioVenta")):0, 
                producto.get("tipoProducto")!=null?producto.get("tipoProducto"):"", 
                producto.get("precioVenta")!=null?Integer.parseInt(producto.get("cantidad")):0, 
                producto.get("precioVenta")!=null?Integer.parseInt(producto.get("alto")):0, 
                producto.get("precioVenta")!=null?Integer.parseInt(producto.get("ancho")):0, 
                producto.get("precioVenta")!=null?Integer.parseInt(producto.get("largo")):0, 
                producto.get("estado")!=null?Integer.parseInt(producto.get("estado")):0);
        if(producto.get("id")!=null){
            if(!producto.get("id").equals("undefined")){
                prod.setId(Integer.parseInt(producto.get("id")));
            }
        }
        result.setData(productosService.save(prod));
        result.setResult(true);
        result.setMessage("");
        result.setError("");
            
        return result;
    }
}
