/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.services;

import com.alexander.diaz.Proyecto.model.entities.Productos;
import com.alexander.diaz.Proyecto.model.entities.Usuarios;
import com.alexander.diaz.Proyecto.services.interfaces.ProductosRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jadiaz
 */
@Service
public class ProductosService {
    
    @Autowired
    ProductosRepository productosRepository;
    
    public List<Productos> findAll() {

        Iterable<Productos> it = productosRepository.getAll(1);

        ArrayList<Productos> productos = new ArrayList<Productos>();
        it.forEach(e -> productos.add(e));

        return productos;
    }
    
    public Productos save(Productos producto){
        producto = productosRepository.save(producto);
        
        return producto;
    }
}
