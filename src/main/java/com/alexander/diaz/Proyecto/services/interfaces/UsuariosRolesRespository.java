/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.services.interfaces;

import com.alexander.diaz.Proyecto.model.entities.Usuarios;
import com.alexander.diaz.Proyecto.model.entities.UsuariosRoles;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jadiaz
 */
public interface UsuariosRolesRespository extends CrudRepository<UsuariosRoles, String>{

    @Query("from UsuariosRoles ur where ur.idUsuario =:id_usuario")
    public Iterable<UsuariosRoles> findByIdUser(@Param("id_usuario") int id_usuario);

}
