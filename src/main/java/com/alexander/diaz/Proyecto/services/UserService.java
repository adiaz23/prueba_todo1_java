/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.services;

import com.alexander.diaz.Proyecto.model.dto.Permisson;
import com.alexander.diaz.Proyecto.model.dto.UserPermisson;
import com.alexander.diaz.Proyecto.model.entities.Usuarios;
import com.alexander.diaz.Proyecto.model.entities.UsuariosRoles;
import com.alexander.diaz.Proyecto.services.interfaces.UsuarioRespository;
import com.alexander.diaz.Proyecto.services.interfaces.UsuariosRolesRespository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jadiaz
 */
@Service
public class UserService {
    
    @Autowired
    UsuarioRespository usuarioRepository;
    
    @Autowired
    UsuariosRolesRespository usuariosRolesRespository;
    
    public List<Usuarios> findAll() {

        Iterable<Usuarios> it = usuarioRepository.findAll();

        ArrayList<Usuarios> users = new ArrayList<Usuarios>();
        it.forEach(e -> users.add(e));

        return users;
    }
    
    public UserPermisson getLogin(String user, String password){
        UserPermisson userPermisson = new UserPermisson();
        Iterable<Usuarios> it = usuarioRepository.getLogin(user, password);
        List<Usuarios> users = new ArrayList<Usuarios>();
        it.forEach(e -> users.add(e));
        if(users.size()>0){
            Usuarios usuario = users.get(0);
            Iterable<UsuariosRoles> itPermissons = usuariosRolesRespository.findByIdUser(usuario.getId());
            List<Permisson> permisos = new ArrayList<Permisson>();
            itPermissons.forEach(e -> {
                Permisson permiso = new Permisson();
                permiso.setId(e.getIdRol());
                permisos.add(permiso);
            });
            userPermisson.setPermissons(permisos);
            userPermisson.setId(usuario.getId());
            userPermisson.setName(usuario.getNombres());
            userPermisson.setLastName(usuario.getApellidos());
            userPermisson.setIdentification(usuario.getIdentificacion());
        }
        return userPermisson;
    }
    
}
