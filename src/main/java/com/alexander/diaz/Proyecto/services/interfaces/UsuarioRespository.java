/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.services.interfaces;

import org.springframework.data.repository.CrudRepository;
import com.alexander.diaz.Proyecto.model.entities.Usuarios;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jadiaz
 */
public interface UsuarioRespository extends CrudRepository<Usuarios, String>{
    
    @Query("from Usuarios u where u.usuario =:usuario and u.clave =:clave")
    public Iterable<Usuarios> getLogin(@Param("usuario") String usuario, @Param("clave") String clave);
}
