/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.services.interfaces;

import com.alexander.diaz.Proyecto.model.entities.Productos;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jadiaz
 */
public interface ProductosRepository  extends CrudRepository<Productos, String>{
    @Query("from Productos p where p.estado =:estado")
    public Iterable<Productos> getAll(@Param("estado") Integer estado);

}
