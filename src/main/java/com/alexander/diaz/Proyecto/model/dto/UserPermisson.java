/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.model.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jadiaz
 */
public class UserPermisson {
       
    private int id;
    private String name;
    private String lastName ;
    private String identification;
    private List<Permisson> permissons;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public List<Permisson> getPermissons() {
            return permissons;
    }

    public void setPermissons(List<Permisson> permissons) {
        this.permissons = permissons;
    }
    
    
}
