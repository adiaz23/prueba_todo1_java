/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.model.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author jadiaz
 */
@Entity
public class Usuarios {
    @Id
    private int id;
    private String nombres;
    private String apellidos;
    private String identificacion;
    private String usuario;
    private String clave;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        //return DigestUtils.sha256Hex(clave);
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Usuarios(String nombres, String apellidos, String identificacion, String usuario, String clave) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.identificacion = identificacion;
        this.usuario = usuario;
        this.clave = clave;
    }
    
    public Usuarios(){
        super();
    }
    
}
