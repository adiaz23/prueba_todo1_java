/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.services;

import com.alexander.diaz.Proyecto.model.entities.Productos;
import com.alexander.diaz.Proyecto.model.entities.Usuarios;
import com.alexander.diaz.Proyecto.services.interfaces.ProductosRepository;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

/**
 *
 * @author jadiaz
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductosServiceTest {
    
    
    @Mock
    private ProductosRepository productosRepository;
    
    @InjectMocks
    private ProductosService productosService;
    
    public ProductosServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findAll method, of class ProductosService.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        
        Iterable<Productos> productosIt = Arrays.asList(new Productos());
        when(productosRepository.findAll()).thenReturn(productosIt);
        List<Productos> result = productosService.findAll();
       
        assertNotNull(result);
    }

    /**
     * Test of save method, of class ProductosService.
     */
    @Test
    public void testSave() {
        System.out.println("save");
        
        Productos producto = new Productos();
        when(productosRepository.save(producto)).thenReturn(producto);
        Productos result = productosService.save(producto);
        
        assertNotNull(result);
    }
    
}
