/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alexander.diaz.Proyecto.services;

import com.alexander.diaz.Proyecto.model.dto.UserPermisson;
import com.alexander.diaz.Proyecto.model.entities.Usuarios;
import com.alexander.diaz.Proyecto.model.entities.UsuariosRoles;
import com.alexander.diaz.Proyecto.services.interfaces.UsuarioRespository;
import com.alexander.diaz.Proyecto.services.interfaces.UsuariosRolesRespository;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

/**
 *
 * @author jadiaz
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceTest {
    @Mock
    private UsuarioRespository usuarioRepository;
    
    @Mock
    private UsuariosRolesRespository usuariosRolesRespository;
    
    @InjectMocks
    private UserService userService;
    
    
    public UserServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findAll method, of class UserService.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");
       
        Iterable<Usuarios> user = Arrays.asList(new Usuarios());
        when(usuarioRepository.findAll()).thenReturn(user);
        List<Usuarios> result = userService.findAll();
       
        assertNotNull(result);
    }

    /**
     * Test of getLogin method, of class UserService.
     */
    @Test
    public void testGetLogin() {
        System.out.println("getLogin");
        String user = "user1";
        String password = "1234";
        String clave = DigestUtils.sha256Hex(password);
        
        Usuarios usuarioModel = new Usuarios();
        usuarioModel.setId(1);
        Iterable<Usuarios> usuariosIt = Arrays.asList(usuarioModel);
        when(usuarioRepository.getLogin(user, clave)).thenReturn(usuariosIt);
        
        UsuariosRoles usuariosRoles = new UsuariosRoles();
        Iterable<UsuariosRoles> usuarioRolesIt = Arrays.asList(usuariosRoles);
        when(usuariosRolesRespository.findByIdUser(usuarioModel.getId())).thenReturn(usuarioRolesIt);
        
        
        UserPermisson result = userService.getLogin(user, password);
       
        assertNotNull(result);
    }
    
}
